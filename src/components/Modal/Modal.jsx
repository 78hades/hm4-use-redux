import ModalWrapper from "./ModalWrapper";
import ModalHeader from "./ModalHeader";
import ModalImage from "../ModalImage/ModalImage";
import ModalClose from "./ModalClose";
import ModalBody from "./ModalBody";
import ModalFooter from "./ModalFooter";
import {
  isShowSecond,
  isShowFirst,
} from "../../redux/showModal.slice/showModal.slice";
import {
  fillBasket,
  clearBasket,
  isBasketAction,
} from "../../redux/basket.slice/basket.slice";
import {
  clearWishlist,
  isWishlistAction,
} from "../../redux/wishlist.slice/wishlist.slice";
import { useSelector, useDispatch } from "react-redux";
function Modal() {
  const dispatch = useDispatch();
  const { isShowModalSecond, isShowModalFirst } = useSelector(
    (state) => state.modal
  );

  const { product } = useSelector((state) => state.product);
  const { isBasket } = useSelector((state) => state.basket);
  const { isWishlist } = useSelector((state) => state.wishlist);
  return (
    <>
      {isShowModalFirst && (
        <ModalWrapper
          onClick={() => {
            dispatch(isShowFirst(false));
          }}
        >
          <div className="modal__content" onClick={(e) => e.stopPropagation()}>
            <ModalClose
              onClick={() => {
                dispatch(isShowFirst(false));
              }}
            ></ModalClose>
            <ModalImage img={product.pathImg}></ModalImage>
            <ModalHeader name={product.name}>Product Delete!</ModalHeader>
            <ModalBody price={product.price}>
              *By clicking the “Yes, Delete” button, {product.name} will be
              deleted.
            </ModalBody>

            <ModalFooter
              firstText="NO, CANCEL"
              firstClick={() => {
                dispatch(isShowFirst(false));
              }}
              secondaryText="YES, DELETE"
              secondaryClick={() => {
                if (isBasket) {
                  dispatch(clearBasket(product));
                  dispatch(isBasketAction(false));
                }
                if (isWishlist) {
                  dispatch(clearWishlist(product));
                  dispatch(isWishlistAction(false));
                }
                dispatch(isShowFirst(false));
              }}
            ></ModalFooter>
          </div>
        </ModalWrapper>
      )}
      {isShowModalSecond && (
        <div>
          <ModalWrapper
            onClick={() => {
              dispatch(isShowSecond(false));
            }}
          >
            <div
              className="modal__content"
              onClick={(e) => e.stopPropagation()}
            >
              <ModalClose
                onClick={() => {
                  dispatch(isShowSecond(false));
                }}
              ></ModalClose>
              <ModalHeader
                img={product.pathImg}
                name={product.name}
              ></ModalHeader>
              <ModalBody
                type={product.type}
                price={product.price}
                color={product.color}
              ></ModalBody>
              <ModalFooter
                firstText="ADD TO CART"
                firstClick={() => {
                  dispatch(fillBasket(product));
                  dispatch(isShowSecond(false));
                  dispatch(isBasketAction(false));
                }}
              ></ModalFooter>
            </div>
          </ModalWrapper>
        </div>
      )}
    </>
  );
}

export default Modal;
