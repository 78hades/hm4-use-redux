import Product from "./BasketProduct";
import Modal from "../../components/Modal/Modal";
import { NavLink } from "react-router-dom";
import { useSelector } from "react-redux";
function BasketProducts() {
  const { basket } = useSelector((state) => state.basket);
  const quantity = basket.reduce((counts, product) => {
    if (!counts[product.id]) {
      counts[product.id] = 1;
    } else {
      counts[product.id] += 1;
    }

    return counts;
  }, {});

  const uniqueArray = basket.reduce((unique, product) => {
    const findProduct = unique.some((item) => item.id === product.id);
    if (!findProduct) {
      unique.push(product);
    }
    return unique;
  }, []);

  const price = basket.reduce((price, product) => {
    if (!price[product.id]) {
      price[product.id] = parseFloat(product.price.slice(1));
    }
    return price;
  }, {});

  const CardProduct = uniqueArray.map((product, index) => (
    <Product
      subtotal={(price[product.id] * quantity[product.id]).toFixed(2)}
      key={index}
      product={product}
      quantity={quantity[product.id]}
    />
  ));
  if (basket.length === 0) {
    return (
      <>
        {" "}
        <p className="cart-page__path">
          <NavLink className="link" to="/">
            Home
          </NavLink>{" "}
          &gt;{" "}
          <NavLink className="link" activeclassname="active" to="/basket">
            {" "}
            Add to cart
          </NavLink>
        </p>
        <div className="cart-page__empty">
          <p className="cart-page__empty-message">
            You have not added any product to cart : &#40; . . . <br />Continue shopping ---&gt;{" "}
            <NavLink className="link-Home" to="/">
              Home
            </NavLink>{" "}
          </p>
        </div>
      </>
    );
  }
  return (
    <>
      {" "}
      <section className="cart-page">
        <p className="cart-page__path">
          <NavLink className="link" to="/">
            Home
          </NavLink>{" "}
          &gt;{" "}
          <NavLink className="link" activeclassname="active" to="/basket">
            {" "}
            Add to cart
          </NavLink>
        </p>
        <div className="cart-page__products-details">
          <h2>products details</h2>
          <p>price</p>
          <p>shipping</p>
          <p>subtotal</p>
          <p>delete</p>
        </div>
        {CardProduct}
      </section>
      <Modal></Modal>
    </>
  );
}

export default BasketProducts;
